from django.db import models


class DataTable(models.Model):
    word_id = models.IntegerField(db_column='WORD_ID')  # Field name made lowercase.
    word_text = models.CharField(db_column='WORD_TEXT', max_length=25)  # Field name made lowercase.
    description = models.CharField(db_column='DESCRIPTION', max_length=50)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'data_table'



