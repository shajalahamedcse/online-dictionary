from django.db import models
from django.contrib.auth.models import User


class DataTable(models.Model):
    word_text = models.CharField(max_length=25)  # Field name made lowercase.
    description = models.CharField(max_length=50)  # Field name made lowercase.

    class Meta:
        db_table = 'data_table'
