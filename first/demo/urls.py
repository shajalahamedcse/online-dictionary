from django.conf.urls import url
from demo import views



urlpatterns = [

url(r'^$',views.index,name='index'),
url(r'^register/$',views.registration_view,name='register'),
url(r'^user_login/$',views.user_login,name='user_login'),

]
