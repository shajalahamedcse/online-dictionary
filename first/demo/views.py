from django.shortcuts import render
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.contrib.auth import views
from django.contrib.auth import authenticate,login,logout
import json
from difflib import get_close_matches
from demo.models import DataTable

# Create your views here.
def index(request):

    c = []
    d= ''
    e = []

    word=''
    if(request.method=="POST"):
        word = request.POST.get('word').lower()

    d = DataTable.objects.values('description').filter(word_text__contains=word)
    print(d)
    if word:
        for i in d:
            for j in i:
                c.append(i[j])

    # return render(request,'demo/find_word.html',{'c':c,'d':d,'e':e})
    return render(request,'demo/find_word.html',{'c':c })


def registration_view(request):

    registered = False

    if(request.method =="POST"):
        name = request.POST.get('username')
        f_name = request.POST.get('first_name')
        l_name = request.POST.get('last_name')
        mail = request.POST.get('email')
        user_password = request.POST.get('password')
        confirm_password = request.POST.get('conpass')

        if(not(User.objects.filter(username = name).exists())):
            if(user_password == confirm_password):
                user = User.objects.create_user(username=name, first_name=f_name, last_name=l_name,
                email=mail,password=user_password)
                user.is_active = True
                user.save()
                registered = True
        else:
            return HttpResponse("<h1>User Name Already Exist</h1>")

    return render(request,'demo/RegistrationTemplate.html',{'registered':registered,})

def user_login(request):

    if(request.method =="POST"):
        username = request.POST.get('username')
        password = request.POST.get('password')

        user = authenticate(username=username,password=password)

        if(user):
            if(user.is_active):
                login(request,user)
                return render(request,'demo/find_word.html',{})
            else:
                return HttpResponse("Account Not Active")
        else:
            return HttpResponse("Invalid Login")

    else:
        return render(request,'login/login.html',{})
